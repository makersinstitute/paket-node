const test = require('tape');
const Paket = require('../lib');

test('should verify packed message', function(t) {
  t.plan(3);

  const message = {
    name: 'John',
    token: 'ABCDE12345'
  };

  console.log('message', message);

  Paket.pack(message, 'pass', {}, function(err, paket) {

    console.log('paket', paket);

    Paket.unpack(paket, 'pass', {}, function(err, messageUnpacked) {

      console.log('message unpacked', messageUnpacked);

      t.ok(!err, err ? err.message : 'ok');
      t.equal(messageUnpacked.name, 'John');
      t.equal(messageUnpacked.token, 'ABCDE12345');
    });

  });
});

test('should verify generated packet message from python', function(t) {
  t.plan(3);

  // this paket valid for 100 years, expect error at that time
  const paket = 'pkt15.1$eyJ0b2tlbiI6IkFCQ0RFMTIzNDUiLCJuYW1lIjoiSm9obiJ9$4605220795$NEUwOTE5MEI5NkYxMTRCNjg5MzFFMTNCN0NBMUZDOUZCOUY3RjAxRjE0RTIyRTMyMkMwQjVGRTU1ODNFODYyNzI4MjI5RTJDQzZBQThCOTc4OTBEOTFFM0M1RTYxN0M0NTIzNkE4MzI5MEZFRUIwQzNEMEYzRjY4MjQ5NTUyMEU='

  Paket.unpack(paket, 'pass', {}, function(err, messageUnpacked) {

    console.log('message unpacked', messageUnpacked);

    t.ok(!err, err ? err.message : 'ok');
    t.equal(messageUnpacked.name, 'John');
    t.equal(messageUnpacked.token, 'ABCDE12345');
  });
})

test('should failed with wrong password', function(t) {
  t.plan(1);

  const message = {
    name: 'John',
    token: 'ABCDE12345'
  };

  console.log('message', message);

  Paket.pack(message, 'pass', {}, function(err, paket) {

    console.log('paket', paket);

    Paket.unpack(paket, 'notpass', {}, function(err, messageUnpacked) {

      console.log('message unpacked', messageUnpacked);

      t.ok(err, err ? err.message : 'ok');
    });

  });
});

test('should failed if expired', function(t) {
  t.plan(1);

  const message = {
    name: 'John',
    token: 'ABCDE12345'
  };

  console.log('message', message);

  Paket.pack(message, 'pass', {}, function(err, paket) {

    console.log('paket', paket);

    setTimeout(function() {
      Paket.unpack(paket, 'pass', {}, function(err, messageUnpacked) {

        console.log('message unpacked', messageUnpacked);

        t.ok(err, err ? err.message : 'ok');
      });
    }, 35000);

  });
});
