
var crypto = require('crypto');
var cryptiles = require('cryptiles');

const prefix = 'pkt15.1'
const separator = '$'
const expireInSeconds = 300

function signString(data, password) {
  const hmac = crypto.createHmac('sha512', password).update(data);
  return digest = hmac.digest('hex').toUpperCase();
}

function verifySignature(data, signature, password) {
  const digest = signString(data, password);
  return cryptiles.fixedTimeComparison(digest.toString(), signature);
}

function verifyTime(expiration) {
  return Math.round(new Date().getTime()/1000) - expiration <= 0;
}

function generateHmac(message, password, callback) {
  const expiration = Math.round(new Date().getTime()/1000) + expireInSeconds;
  const encodedMessage = new Buffer(JSON.stringify(message)).toString('base64');
  const payload = prefix + separator + encodedMessage + separator + expiration;
  const signature = signString(payload, password);
  const encodedSignature = new Buffer(JSON.stringify(signature)).toString('base64');
  return callback(null, payload + separator + encodedSignature);
}

exports.pack = function(message, password, options, callback) {
  return generateHmac(message, password, callback);
}

exports.unpack = function(message, password, options, callback) {
  const parts = message.split(separator);
  if (parts.length !== 4) {
    return callback(new Error('Incorrect paket parts'))
  }

  const macPrefix = parts[0];
  const encodedMessage = parts[1];
  const expiration = parts[2];
  const encodedSignature = parts[3];
  const payload = macPrefix + separator + encodedMessage + separator + expiration;
  const decodedSignature = new Buffer(encodedSignature, 'base64').toString('binary').replace(/"/g, '');

  if (!verifySignature(payload, decodedSignature, password)) {
    return callback(new Error('Invalid signature'));
  }

  if (!verifyTime(expiration)) {
    return callback(new Error('Expired'));
  }

  const decodedMessage = new Buffer(encodedMessage, 'base64').toString('binary');

  try {
    const object = JSON.parse(decodedMessage);
    callback(null, object);
  } catch (exception) {
    callback(new Error('Decoded message is not a valid JSON object'));
  }
}
