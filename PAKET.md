
# Paket format data

```
header | separator | base64 payload | separator | expiration | separator | base64 digest
```

For example: 

```
pkt15.1$eyJuYW1lIjoiSm9obiIsInRva2VuIjoiQUJDREUxMjM0NSJ9$1449330440$IllrTEt4d0lubzRWdGZ6a2FNeEFTZTl4R2EzRGxUUnpwOGZldGRpZnBVYTd0cEpmNHVTWTEyS21iWDFkZW16YzVkdnJ0eWhwMnhnWEd6NGRscmdvTDN3Ig==
```

Separator is `$`
header is `pkt15.1` where `pkt15` is Paket identifier and `1` is version

```
pkt15.1 $ eyJuYW1lIjoiSm9obiIsInRva2VuIjoiQUJDREUxMjM0NSJ9 $ 1449330440 $ IllrTEt4d0lubzRWdGZ6a2FNeEFTZTl4R2EzRGxUUnpwOGZldGRpZnBVYTd0cEpmNHVTWTEyS21iWDFkZW16YzVkdnJ0eWhwMnhnWEd6NGRscmdvTDN3Ig==
```
