
# Currently only HMAC, encrypt will be added soon!

# Paket

Encrypt then HMAC message content

Aim is to provide compatible implementation for some programming languages: 

* Nodejs (this module) [paket-node](https://gitlab.com/makersinstitute/paket-node)
* Python [paket-python](https://gitlab.com/makersinstitute/paket-python)
* Contribute more here

See [Paket](PAKET.md) for the standard

# Install

```sh
npm install git+https://gitlab.com/makersinstitute/paket-node
```

# Methods

## pack

```javascript
pack(message, password, options, callback)
```

## unpack

```javascript
unpack(message, password, options, callback)
```

* `message` JSON object containing data
* `password` string password
* `options` for packing. currently not used
* `callback` with params `(err, result)` with `result` can be packed mesage or unpacked message depending on operation

# Example

```javascript
const Paket = require('paket-node');

const message = {
    name: 'John',
    token: 'ABCDE12345'
};

Paket.pack(message, 'pass', {}, function(err, paket) {

    Paket.unpack(paket, 'pass', {}, function(err, messageUnpacked) {

    });

});
```

# License

MIT